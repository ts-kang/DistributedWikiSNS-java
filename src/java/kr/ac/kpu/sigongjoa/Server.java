package kr.ac.kpu.sigongjoa;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class Server {
	private Database db;
	private String path;
	private String domain;
	private String passwd;
	private String termsOfUse;
	private Policy managementPolicy;

	public Server() {
		managementPolicy = new Policy();
		db = new Database();
	}

	public Server(String domain) {
		setDomain(domain);
		managementPolicy = new Policy(domain);
		db = new Database(domain);
	}

	public String getPath() {
		return path;
	}

	public void setPath(String domain) {
		path = "servers/" + domain + "/";
	}

	public User createUser(String id, String passwd, String nickname) {
		User user = new User(domain);

		user.setId(id);
		user.setPasswd(passwd);
		user.setNickname(nickname);

		int line = 1;
		try(BufferedReader in = Files.newBufferedReader(Paths.get("servers/servertable"))) {
			String buf;
			for(; (buf = in.readLine()) != null; ++line) {
				if(buf.equals(id)) {
					return null;
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}

		user.setUserid(line);
		db.addUser(user);

		try(PrintWriter out = new PrintWriter(Files.newBufferedWriter(Paths.get(path + "__userlist__"), StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
			out.println(user.getId());
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}

		saveUser(user);

		return user;
	}

	public int loadServer() {
		try(BufferedReader in = Files.newBufferedReader(Paths.get("servers/servertable"))) {
			String buf;
			while((buf = in.readLine()) != null) {
				buf = buf.trim();
				if(buf.equals(domain)) {
					return
						loadPasswd() << 1 |
						loadTermsOfUse() << 2 |
						loadPolicy() << 3 |
						loadUsers() << 4 |
						loadContents() << 5;
				}
			}
		} catch(IOException e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int loadPasswd() {
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + "serverinfo/passwd"))) {
			passwd = in.readLine().trim();
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return 1;
	}

	public int loadTermsOfUse() {
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + "serverinfo/termsofuse"))) {
			String buf;
			while((buf = in.readLine()) != null) {
				termsOfUse += buf;
			}
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return 1;
	}

	public int loadPolicy() {
		managementPolicy.setDomain(domain);
		managementPolicy.loadForbiddenWords();
		managementPolicy.loadCautionReasons();
		managementPolicy.loadBanReasons();
		return 0;
	}

	public int loadUser(int uid) {
		return db.loadUser(uid);
	}

	public int loadUser(String id) {
		return db.loadUser(id);
	}

	public int loadUsers() {
		return db.loadUsers();
	}

	public Database getDb() {
		return db;
	}

	public int loadContent(int cid) {
		return db.loadContent(cid);
	}

	public int loadContent(String title) {
		return db.loadContent(title);
	}

	public int loadContents() {
		return db.loadContents();
	}

	public int saveServer() {
		return
			savePasswd() |
			saveTermsOfUse() << 1 |
			saveManagementPolicy() << 2 |
			saveUsers() << 3 |
			saveContents() << 4;
	}

	public int savePasswd() {
		try(PrintWriter out = new PrintWriter(Files.newBufferedWriter(Paths.get(path + "serverinfo/passwd"), StandardOpenOption.CREATE))) {
			out.println(passwd);
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return 1;
	}

	public int saveTermsOfUse() {
		try(PrintWriter out = new PrintWriter(Files.newBufferedWriter(Paths.get(path + "serverinfo/termsofuse"), StandardOpenOption.CREATE))) {
			out.println(termsOfUse);
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return 1;
	}

	public int saveManagementPolicy() {
		return managementPolicy.savePolicy(domain);
	}

	public int saveUser(User u) {
		return db.saveUser(u);
	}

	public int saveUsers() {
		return db.saveUsers();
	}

	public int saveContent(Content c) {
		return db.saveContent(c);
	}

	public int saveContents() {
		return db.saveContents();
	}

	public void reset() {
		domain = "";
		passwd = "";
		termsOfUse = "";
		managementPolicy = null;
		db = null;
	}

	public int checkPasswd(String input) {
		return passwd.compareTo(input);
	}

	public void setDb(Database db) {
		this.db = db;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
		setPath(domain);
		db.setDomain(domain);
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getTermsOfUse() {
		return termsOfUse;
	}

	public void setTermsOfUse(String termsOfUse) {
		this.termsOfUse = termsOfUse;
	}

	public Policy getManagementPolicy() {
		return managementPolicy;
	}

	public void setManagementPolicy(Policy managementPolicy) {
		this.managementPolicy = managementPolicy;
	}

}
