package kr.ac.kpu.sigongjoa;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class Policy {
	private String domain;
	private List<String> forbiddenWords = new ArrayList<>();
	private List<String> cautionReasons = new ArrayList<>();
	private List<String> banReasons = new ArrayList<>();
	private String path;

	public Policy() {}

	public Policy(String domain) {
		setDomain(domain);
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
		setPath(domain);
	}

	public List<String> getForbiddenWords() {
		return forbiddenWords;
	}

	public void setForbiddenWords(List<String> forbiddenWords) {
		this.forbiddenWords = forbiddenWords;
	}

	public List<String> getCautionReasons() {
		return cautionReasons;
	}

	public void setCautionReasons(List<String> cautionReasons) {
		this.cautionReasons = cautionReasons;
	}

	public List<String> getBanReasons() {
		return banReasons;
	}

	public void setBanReasons(List<String> banReasons) {
		this.banReasons = banReasons;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String domain) {
		this.path = "servers/" + domain + "/serverinfo/policy/";
	}

	public int loadPolicy() {
		return
			loadForbiddenWords() << 2 |
			loadCautionReasons() << 1 |
			loadBanReasons();
	}

	public int loadPolicy(String domain) {
		setDomain(domain);
		return loadPolicy();
	}

	public int loadForbiddenWords() {
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + "forbiddenWords"))) {
			String buf;
			while((buf = in.readLine()) != null) {
				addForbiddenWord(buf.trim());
			}
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return 1;
	}

	public int loadCautionReasons() {
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + "cautionReasons"))) {
			String buf;
			while((buf = in.readLine()) != null) {
				addCautionReason(buf.trim());
			}
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return 1;
	}

	public int loadBanReasons() {
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + "banReasons"))) {
			String buf;
			while((buf = in.readLine()) != null) {
				addBanReason(buf.trim());
			}
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return 1;
	}

	public int savePolicy() {
		return
			saveForbiddenWords() << 2 |
			saveCautionReasons() << 1 |
			saveBanReasons();
	}

	public int savePolicy(String domain) {
		setDomain(domain);
		return savePolicy();
	}

	public int saveForbiddenWords() {
		try(PrintWriter out = new PrintWriter(Files.newBufferedWriter(Paths.get(path + "forbiddenWords"), StandardOpenOption.CREATE))) {
			for(String e : forbiddenWords) {
				out.println(e);
			}
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return 1;
	}

	public int saveCautionReasons() {
		try(PrintWriter out = new PrintWriter(Files.newBufferedWriter(Paths.get(path + "cautionReasons"), StandardOpenOption.CREATE))) {
			for(String e : cautionReasons) {
				out.println(e);
			}
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return 1;
	}

	public int saveBanReasons() {
		try(PrintWriter out = new PrintWriter(Files.newBufferedWriter(Paths.get(path + "banReasons")))) {
			for(String e : banReasons) {
				out.println(e);
			}
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return 1;
	}

	public void addForbiddenWord(String s) {
		forbiddenWords.add(s);
	}

	public void addCautionReason(String s) {
		cautionReasons.add(s);
	}

	public void addBanReason(String s) {
		banReasons.add(s);
	}

	public void delForbiddenWord(int lineNum) {
		forbiddenWords.remove(lineNum - 1);
	}

	public void delCautionReason(int lineNum) {
		cautionReasons.remove(lineNum - 1);
	}

	public void delBanReason(int lineNum) {
		banReasons.remove(lineNum - 1);
	}

	@Override
	public String toString() {
		String out = "";
		out += "---forbiddenWords---\n";
		for(String s : forbiddenWords) {
			out += s + "\n";
		}
		out += "\n";
		out += "---cautionReasons---\n";
		for(String s : cautionReasons) {
			out += s + "\n";
		}
		out += "\n";
		out += "---banReasons---\n";
		for(String s : banReasons) {
			out += s + "\n";
		}
		out += "\n";

		return out;
	}
}
