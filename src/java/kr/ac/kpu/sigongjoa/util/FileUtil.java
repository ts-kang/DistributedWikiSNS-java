package kr.ac.kpu.sigongjoa.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

public class FileUtil {
	@Nullable
	public static String readFirstLine(@NotNull String path) throws IOException {
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path))) {
			return in.readLine();
		}
	}
}
