package kr.ac.kpu.sigongjoa;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class UserClient implements Client {
	private final String prompt = " @ ";

	private Scanner sc;

	private User curUser;
	private Server curServer;

	@Override
	public void	run() {
		sc = new Scanner(System.in);
		try {
			for(;;) {
				System.out.print(prompt);
				switch(sc.nextLine()) {
					case "help": help();
						break;
					case "connect": connect();
						break;
					case "disconnect": disconnect();
						break;
					case "signin": signin();
						break;
					case "signout": signout();
						break;
					case "signup": signup();
						break;
					case "serverinfo": serverinfo();
						break;
					case "userinfo": userinfo();
						break;
					case "viewcontent": viewcontent();
						break;
					case "createcontent": createcontent();
						break;
					case "editcontent": editcontent();
						break;
					case "addcomment": addcomment();
						break;
					case "timeline": timeline();
						break;
					case "editprofile": editprofile();
						break;
					case "setting": setting();
						break;
					case "exit":
						return;
				}
			}
		} finally {
			sc.close();
		}
	}

	@Override
	public int help() {
		System.out.println("help : show this page.");
		System.out.println("connect : connect to server");
		System.out.println("disconnect : disconnect from current server.");
		System.out.println("signin : sign in.");
		System.out.println("signout : sign out.");
		System.out.println("signup : sign up.");
		System.out.println("viewcontent : view this server's content.");
		System.out.println("createcontent : create new content.");
		System.out.println("editcontent : edit existing content.");
		System.out.println("addcomment : add comment to existing content.");
		System.out.println("timeline : show you current User's timeline.");
		System.out.println("serverinfo : show you current Server's information.");
		System.out.println("userinfo : show you current User's information.");
		System.out.println("setting : edit current User's information.");
		System.out.println("exit : exit program.");
		return 0;
	}

	@Override
	public int connect() {
		if(curServer == null) {
			System.out.print("server domain : ");
			String buf = sc.nextLine();
			curServer = new Server();
			curServer.setDomain(buf);
			if(curServer.loadServer() == 0) {
				return 0;
			} else {
				curServer = null;
				System.out.println("server wasn't loaded");
				return 1;
			}
		} else {
			System.out.println("You already connected to server");
			return 1;
		}
	}

	@Override
	public int disconnect() {
		if(curServer != null) {
			curUser = null;
			curServer = null;
			System.out.println("Disconnected from server successfully.");
			return 0;
		}

		System.out.println("There is no connection.");
		return 1;
	}

	public int signin() {
		if(curServer == null) {
			System.out.println("There is no connection.");
			return 1;
		}

		if(curUser != null) {
			System.out.println("You've already signed in.");
			return 1;
		}

		String idBuf;
		String passwdBuf;
		System.out.print("id : ");
		idBuf = sc.nextLine().trim();
		User u = new User(curServer.getDomain());
		if(u.loadUser(idBuf) == 0) {
			for(int i = 0; i < 5; ++i) {
				System.out.print("password : ");
				passwdBuf = sc.nextLine();
				if(u.checkPasswd(passwdBuf) == 0) {
					System.out.println("login succeeded.");
					curUser = u;
					return 0;
				} else {
					System.out.println("Password incorrect.");
				}
			}
			System.out.println("You've entered the wrong password too many times.");
			return 1;
		}
	}

	public int signout() {
		if(curServer == null) {
			System.out.println("There is no connection.");
			return 1;
		}

		if(curUser == null) {
			System.out.println("You haven't logged in.");
			return 1;
		}

		curUser = null;
		System.out.println("Signed out successfully");
		return 0;
	}

	public int signup() {
		if(curServer == null) {
			System.out.println("There is no connection.");
			return 1;
		}

		String idBuf;
		String passwdBuf;
		String nicknameBuf;
		System.out.print("id : ");
		idBuf = sc.nextLine().trim();
		System.out.print("password : ");
		passwdBuf = sc.nextLine().trim();
		for(int i = 0;; ++i) {
			String retypeBuf;
			System.out.print("retype password : ");
			retypeBuf = sc.nextLine();
			if(passwdBuf.equals(retypeBuf)) {
				break;
			}
			if(i == 5) {
				System.out.println("Timeout error");
				return 1;
			}
		}
		System.out.print("nickname : ");
		nicknameBuf = sc.nextLine();
		File dir = new File(curServer.getPath() + "database/users/" + idBuf);
		dir.mkdirs();
		curServer.createUser(idBuf, passwdBuf, nicknameBuf);
		return 0;
	}

	public int serverinfo() {
		if(curServer == null) {
			System.out.println("There is no connection.");
			return 1;
		}

		System.out.println("domain : " + curServer.getDomain());
		System.out.println("Terms Of Use : " + curServer.getTermsOfUse());
		System.out.println("Policy :");
		System.out.println(curServer.getManagementPolicy());
		return 0;
	}
	
	public int userinfo() {
		if(curServer == null) {
			System.out.println("There is no connection.");
			return 1;
		}

		if(curUser == null) {
			System.out.println("You haven't logged in.");
			return 1;
		}

		System.out.println("id : " + curUser.getId());
		System.out.println("nickname : " + curUser.getNickname());
		System.out.println("profile :");
		System.out.println(curUser.getProfile());
		System.out.println("-followers");
		for(String follower : curUser.getFollowers()) {
			System.out.println(follower);
		}
		System.out.println("-followings");
		for(String following : curUser.getFollowings()) {
			System.out.println(following);
		}

		return 0;
	}

	public int viewcontent() {
		if(curServer == null) {
			System.out.println("There is no connection.");
			return 1;
		}

		if(curUser == null) {
			System.out.println("You haven't logged in.");
			return 1;
		}

		String titleBuf;
		String domainBuf;
		System.out.print("domain : ");
		String domainBuf = sc.nextLine();
		System.out.print("title : ");
		titleBuf = sc.nextLine().trim();

		Content c = new Post(domainBuf);
		if(c.loadContent(titleBuf) == 0) {
			System.out.println(c);
			return 0;
		}

		System.out.println("Content doesn't exist.");
		return 1;
	}

	public int createcontent() {
		if(curServer == null) {
			System.out.println("There is no connection.");
			return 1;
		}

		if(curUser == null) {
			System.out.println("You haven't logged in.");
			return 1;
		}
		
		String title;
		System.out.print("title : ");
		title = sc.nextLine();
		Content c;
		String owner = curUser.getId();
		String dayOfCreation = new Date().toString();
		Editor e = new Editor();
		e.edit("");
		String text = e.getBuffer();
		File dir = new File("servers/" + curServer.getDomain() + "/database/contents/" + title);
		dir.mkdirs();
		String curDomain = curUser.getDomain();
		curServer.createContent(title, owner, dayOfCreation, text);
		for(String follower : curUser.getFollowers()) {
			if(follower.startsWith("&")) {
				User u = new User(follower.substring(1, follower.indexOf(':')));
				u.loadUser(follower.substring(follower.indexOf(':') + 1));
				u.addTimeline(c);
				u.saveUser();
			} else {
				User u = new User(curDomain);
				u.loadUser(follower);
				u.addTimeline(c);
				u.saveUser();
			}
		}
		return 0;
	}

	public int editcontent() {
		if(curServer == null) {
			System.out.println("There is no connection.");
			return 1;
		}

		if(curUser == null) {
			System.out.println("You haven't logged in.");
			return 1;
		}

		String titleBuf;
		System.out.print("content title : ");
		titleBuf = sc.nextLine().trim();
		Content c = new Post(curServer.getDomain());
		if(c.loadContent(titleBuf) == 0) {
			if(c.getOwner().compare(curUser.getId())) {
				Editor e = new Editor();
				e.edit(c.getText());
				c.setText(e.getBuffer());
				c.saveText();
				return 0;
			}
		} else {
			System.out.println("Content doesn't exist.");
			return 1;
		}
	}

	public int addcomment()  {
		if(curServer == null) {
			System.out.println("There is no connection.");
			return 1;
		}

		if(curUser == null) {
			System.out.println("You haven't logged in.");
			return 1;
		}

		String domainBuf;
		String titleBuf;
		System.out.print("server domain : ");
		domainBuf = sc.nextLine();
		System.out.print("title : ");
		titleBuf = sc.nextLine().trim();
		Content c = new Post(domainBuf);
		if(c.loadContent(titleBuf) == 0) {
			String title;
			String owner = curUser.getId();
			String dayOfCreation = new Date().toString();
			String text;
			System.out.print("comment title : ");
			title = sc.nextLine().trim();
			Editor e = new Editor();
			e.edit("");
			text = e.getBuffer();
			File dir = new File("servers/" + curServer.getDomain() + "/database/contents/" + title);
			dir.mkdirs();
			Comment comment = curServer.createContent(title, owner, dayOfCreation, text);
			if(comment != null) {
				comment.setOriginal(c);
				comment.saveOriginal();
				c.addComment(comment);
				c.saveComments();
				return 0;
			} else {
				System.out.println("cannot create comment.");
				return 1;
			}
		} else {
			System.out.println("Content doesn't exist.");
			return 1;
		}
	}

	public int timeline() {
		if(curServer == null) {
			System.out.println("There is no connection.");
			return 1;
		}

		if(curUser == null) {
			System.out.println("You haven't logged in.");
			return 1;
		}

		for(Content c : curUser.getTimeline()) {
			System.out.println(c);
			System.out.println("-----------------------");
		}

		return 0;
	}

	public int follow() {
		if(curServer == null) {
			System.out.println("There is no connection.");
			return 1;
		}

		if(curUser == null) {
			System.out.println("You haven't logged in.");
			return 1;
		}

		String domainBuf;
		String idBuf;
		System.out.print("server domain : ");
		domainBuf = sc.nextLine();
		System.out.print("id : ");
		idBuf = sc.nextLine();
		User u = new User(domainBuf);
		if(u.loadUser(idBuf) == 0) {
			if(u.getDomain().equals(curUser.getDomain())) {
				curUser.addFollowing(idBuf);
				u.addFollower(curUser.getId());
			} else {
				curUser.addFollowing("&" + domainBuf + ":" + idBuf);
				u.addFollower("&" + curUser.getDomain() + ":" + curUser.getId());
			}
			curUser.saveFollowings();
			u.saveFollowers();
			return 0;
		} else {
			System.out.println("User doesn't exist.");
			return 1;
		}
	}

	public int editprofile() {
		if(curServer == null) {
			System.out.println("There is no connection.");
			return 1;
		}

		if(curUser == null) {
			System.out.println("You haven't logged in.");
			return 1;
		}

		Editor e = new Editor();
		e.edit(curUser.getProfile());
		curUser.setProfile(e.getBuffer());
		return curUser.saveProfile();
	}

	public int setting() {
		if(curServer == null) {
			System.out.println("There is no connection.");
			return 1;
		}

		if(curUser == null) {
			System.out.println("You haven't logged in.");
			return 1;
		}

		String buf;
loop:
		for(;;) {
			System.out.print("setting command : ");
			buf = sc.nextLine();
			switch(buf) {
				case "help":
					System.out.println("changepasswd : change current user's password");
					System.out.println("changenickname : change current user's nickname");
					System.out.println("exit : exit setting manager");
					break;
				case "changepasswd":
					System.out.print("current password : ");
					buf = sc.nextLine();
					if(curUser.checkPasswd(buf) == 0) {
						String passwdBuf;
						System.out.print("new password : ");
						buf = sc.nextLine();
						System.out.print("retype new password : ");
						passwdBuf = sc.nextLine();
						if(buf.equals(passwdBuf)) {
							curUser.setPasswd(buf);
						} else {
							System.out.println("Passwords do not match.");
						}
					} else {
						System.out.println("Password incorrect");
					}
					break;
				case "changenickname":
					System.out.print("new nickname : ");
					buf = sc.nextLine();
					curUser.setNickname(buf);
					break;
				case "exit":
					System.out.println("exit setting manager");
					break loop;
			}
		}
		return curUser.saveUser();
	}

	public int exit() {
		System.exit(0);
		return 0;
	}

	public static void main(String[] args) {
		Client userClient = new UserClient();
		userClient.run();
	}
}
