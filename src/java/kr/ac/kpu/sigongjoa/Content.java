package kr.ac.kpu.sigongjoa;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class Content {
	private int contentid;
	private String domain;
	private String title;
	private String owner;
	private String dayOfCreation;
	private String text;
	private List<Content> comments;
	private String path;

	public Content() {}

	public Content(int cid) {
		contentid = cid;
	}

	public Content(String domain) {
		setDomain(domain);
	}

	public int getContentid() {
		return contentid;
	}

	public void setContentid(int contentid) {
		this.contentid = contentid;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
		setPath(domain);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getOwner() {
		return owner;
	}

	public String getDayOfCreation() {
		return dayOfCreation;
	}

	public void setDayOfCreation(String dayOfCreation) {
		this.dayOfCreation = dayOfCreation;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<Content> getComments() {
		return comments;
	}

	public void setComments(List<Content> comments) {
		this.comments = comments;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String domain) {
		this.path = "servers/" + domain + "/database/contents/";
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public int loadContent() {
		return
			loadDayOfCreation() |
			loadOwner() << 1 |
			loadText() << 2 |
			loadComments() << 3;
	}

	public int loadContent(int cid) {
		contentid = cid;
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + "__contentlist__"))) {
			String buf;
			for(int line = 1; (buf = in.readLine()) != null; ++line) {
				if(line == cid) {
					title = buf.trim();
					break;
				}
			}

			return loadContent();
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int loadContent(String title) {
		setTitle(title);
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + "__contentlist__"))) {
			String buf;
			for(int line = 1; (buf = in.readLine()) != null; ++line) {
				if(buf.equals(title)) {
					contentid = line;
					break;
				}
			}

			return loadContent();
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int loadDayOfCreation() {
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + title + "/dayOfCreation"))) {
			dayOfCreation = in.readLine().trim();

			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int loadOwner() {
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + title + "/owner"))) {
			owner = in.readLine().trim();

			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int loadText() {
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + title + "/text"))) {
			String buf;
			while((buf = in.readLine()) != null) {
				text += buf + "\n";
			}

			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int loadComments() {
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + title + "/comments"))) {
			String buf;
			Content c;
			while((buf = in.readLine()) != null) {
				buf = buf.trim();

				if(buf.startsWith("&")) {
					c = new Content(buf.substring(1, buf.indexOf(':')));
					c.loadContent(buf.substring(buf.indexOf(':') + 1));
				} else {
					c = new Content(domain);
					c.loadContent(buf);
				}
				comments.add(c);
			}

			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int saveContent() {
		return
			saveDayOfCreation() |
			saveOwner() << 1 |
			saveText() << 2 |
			saveComments() << 3;
	}

	public int saveContent(String domain) {
		setDomain(domain);
		return saveContent();
	}

	public int saveDayOfCreation() {
		try(PrintWriter out = new PrintWriter(Files.newBufferedWriter(Paths.get(path + title + "/dayOfCreation"), StandardOpenOption.CREATE))) {
			out.println(dayOfCreation);
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int saveOwner() {
		try(PrintWriter out = new PrintWriter(Files.newBufferedWriter(Paths.get(path + title + "/owner"), StandardOpenOption.CREATE))) {
			out.println(owner);
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int saveText() {
		try(PrintWriter out = new PrintWriter(Files.newBufferedWriter(Paths.get(path + title + "/text"), StandardOpenOption.CREATE))) {
			out.println(text);
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int saveComments() {
		try(PrintWriter out = new PrintWriter(Files.newBufferedWriter(Paths.get(path + title + "/comments"), StandardOpenOption.CREATE))) {
			for(Content c : comments) {
				if(c.getDomain().equals(domain)) {
					out.println(c.getTitle());
				} else {
					out.println("&" + c.getDomain() + ":" + c.getTitle());
				}
			}
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public void addComment(Content comment) {
		comments.add(comment);
	}

	public void delComment(int cid) {
		for(Content c : comments) {
			if(c.getContentid() == cid) {
				comments.remove(c);
			}
		}
	}

	public void delComment(String title) {
		for(Content c : comments) {
			if(c.getTitle().equals(title)) {
				comments.remove(c);
			}
		}
	}

	public void delComment(Content comment) {
		comments.remove(comment);
	}

	@Override
	public String toString() {
		String ret = "";
		ret += "title : " + title + "\n";
		ret += "owner : " + owner + "\n";
		ret += "dayofcreation : " + dayOfCreation + "\n";
		ret += "text : " + text + "\n";
		ret += "-Comments\n";
		for(Content c : comments) {
			ret += c;
		}

		return ret;
	}
}
