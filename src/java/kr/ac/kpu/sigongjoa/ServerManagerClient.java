package kr.ac.kpu.sigongjoa;

import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;

public class ServerManagerClient implements Client {
	private static final String prompt = " & ";

	private Scanner sc;

	private Server curServer;
	private Policy curPolicy;
	private String curTOU;

	@Override
	public void run() {
		try {
			sc = new Scanner(System.in);
loop:
			for(;;) {
				System.out.print(prompt);
				switch(sc.nextLine()) {
					case "help": help();
						break;
					case "connect": connect();
						break;
					case "disconnect": disconnect();
						break;
					case "newserver": newserver();
						break;
					case "serverinfo": serverinfo();
						break;
					case "changepasswd": changepasswd();
						break;
					case "viewpolicy": viewpolicy();
						break;
					case "addpolicy": addpolicy();
						break;
					case "delpolicy": delpolicy();
						break;
					case "touedit": touedit();
						break;
					case "exit":
						break loop;
					default:
						System.out.println("unknown command.");
						break;
				}
			}
		} finally {
			sc.close();
		}
	}

	@Override
	public int help() {
		System.out.println("help : show this page.");
		System.out.println("connect : connect to server.");
		System.out.println("disconnect : disconnect to server.");
		System.out.println("newserver : create new server.");
		System.out.println("serverinfo : show you current Server's informaion.");
		System.out.println("changepasswd : change current Server's password.");
		System.out.println("viewpolicy : show you current Server's policy.");
		System.out.println("addpolicy : add new policy to current Server.");
		System.out.println("delpolicy : del existing policy from current Server.");
		System.out.println("touedit : edit current Server's Terms Of Use.");
		System.out.println("exit : exit program.");
		return 0;
	}

	@Override
	public int connect() {
		if(curServer == null) {
			String buf;
			System.out.print("server domain : ");
			buf = sc.nextLine();
			curServer = new Server();
			curServer.setDomain(buf);
			if(curServer.loadServer() == 0) {
				String passwdBuf;
				System.out.print("server password : ");
				passwdBuf = sc.nextLine();
				if(curServer.checkPasswd(passwdBuf) == 0) {
					System.out.println("server was loaded successfully.");
					curPolicy = curServer.getManagementPolicy();
					curTOU = curServer.getTermsOfUse();
					return 0;
				} else {
					System.out.println("password incorrect.");
					curServer = null;
					return 1;
				}
			}

			System.out.println("server wasn't loaded");
			return 1;
		}

		System.out.println("server wasn't loaded");
		return 1;
	}

	@Override
	public int disconnect() {
		if(curServer == null) {
			System.out.println("There is no connection.");
			return 1;
		}

		curServer = null;
		System.out.println("Disconnected from server successfully.");
		return 0;
	}

	public int newserver() {
		Server newsrv = new Server();
		System.out.print("Server domain : ");
		String domain = sc.nextLine();
		System.out.print("Password : ");
		String passwd = sc.nextLine();
		String passwd2 = "";

		for(int i = 0; !passwd.equals(passwd2); ++i) {
			if(i > 5) {
				return 1;
			}

			System.out.print("retype password");
			passwd2 = sc.nextLine();
		}

		newsrv.setPasswd(passwd);
		new File(newsrv.getPath()).mkdirs();
		new File(newsrv.getPath() + "/serverinfo").mkdirs();
		new File(newsrv.getPath() + "/database").mkdirs();
		try(PrintWriter out = new PrintWriter(Files.newBufferedWriter(Paths.get("servers/servertable"), StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
			out.println(domain);
			saveServer();
			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int serverinfo() {
		if(curServer == null) {
			System.out.println("There is no connection.");
			return 1;
		}

		System.out.println("domain : " + curServer.getDomain());
		System.out.println("Terms Of Use :");
		System.out.println(curServer.getTermsOfUse());
		System.out.println("Policy :");
		System.out.println(curServer.getManagementPolicy());
		return 0;
	}

	public int changepasswd() {
		if(curServer == null) {
			System.out.println("There is no connection.");
			return 1;
		}

		System.out.print("input current password : ");
		String passwdBuf = sc.nextLine();

		if(curServer.checkPasswd(passwdBuf) == 0) {
			System.out.print("input new password : ");
			passwdBuf = sc.nextLine();
			for (int i = 0; i < 5; ++i) {
				System.out.print("retype new password : ");
				String checkBuf = sc.nextLine();
				if(passwdBuf.equals(checkBuf)) {
					curServer.setPasswd(passwdBuf);
					break;
				}
				if(i == 4) {
					System.out.println("Timeout error.");
					return 1;
				}
			}

			return 0;
		}

		System.out.println("Password incorrect.");
		return 1;
	}

	public int viewpolicy() {
		if(curServer == null) {
			System.out.println("There is no connection.");
			return 1;
		}

		System.out.println(curPolicy);
		return 0;
	}

	public int addpolicy() {
		if(curServer == null) {
			System.out.println("There is no connection.");
			return 1;
		}

loop:
		for(;;) {
			System.out.print(" ? ");
			switch(sc.nextLine()) {
				case "help":
					System.out.println("1. forbiddenword");
					System.out.println("2. cautionreason");
					System.out.println("3. banreason");
					System.out.println("4. view");
					System.out.println("5. exit");
					break;
				case "forbiddenword":
					System.out.print("input : ");
					curPolicy.addForbiddenWord(sc.nextLine().trim());
					break;
				case "cautionreason":
					System.out.print("input : ");
					curPolicy.addCautionReason(sc.nextLine().trim());
					break;
				case "banreason":
					System.out.print("input : ");
					curPolicy.addBanReason(sc.nextLine().trim());
					break;
				case "view":
					System.out.println(curPolicy);
					break;
				case "exit":
					curPolicy.savePolicy();
					break loop;
			}
		}
		return 0;
	}

	public int delpolicy() {
		if(curServer == null) {
			System.out.println("There is no connection.");
			return 1;
		}

loop:
		for(;;) {
			System.out.print(" ? ");
			switch(sc.nextLine()) {
				case "help":
					System.out.println("1. forbiddenword");
					System.out.println("2. cautionreason");
					System.out.println("3. banreason");
					System.out.println("4. view");
					System.out.println("5. exit");
					break;
				case "forbiddenword":
					System.out.print("input : ");
					curPolicy.delForbiddenWord(Integer.parseInt(sc.nextLine()));
					break;
				case "cautionreason":
					System.out.print("input : ");
					curPolicy.delCautionReason(Integer.parseInt(sc.nextLine()));
					break;
				case "banreason":
					System.out.print("input : ");
					curPolicy.delBanReason(Integer.parseInt(sc.nextLine()));
					break;
				case "view":
					System.out.println(curPolicy);
					break;
				case "exit":
					curPolicy.savePolicy();
					break loop;
			}
		}
		return 0;
	}

	public int touedit() {
		if(curServer == null) {
			System.out.println("There is no connection.");
			return 1;
		}
		
		Editor e = new Editor(sc);
		e.edit(curServer.getTermsOfUse());
		curServer.setTermsOfUse(e.getBuffer());
		return curServer.saveTermsOfUse();
	}

	public static void main(String[] args) {
		Client sec = new ServerManagerClient();
		sec.run();
	}
}
