package kr.ac.kpu.sigongjoa;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Database {
	private String domain;
	private List<User> allUsers;
	private List<Content> allContents;
	private String path;

	public Database() {}

	public Database(String domain) {
		setDomain(domain);
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
		setPath(domain);
	}

	public String getPath() {
		return path;
	}

	public void setPath(String domain) {
		path = "servers/" + domain + "/database/";
	}

	public void addUser(User u) {
		allUsers.add(u);
	}

	public void addContent(Content c) {
		allContents.add(c);
	}

	public int loadUser(int userid) {
		User u = new User();

		if(u.loadUser(userid) == 0) {
			allUsers.add(u);
			return 0;
		}

		return 1;
	}

	public int loadUser(String id) {
		User u = new User();

		if(u.loadUser(id) == 0) {
			allUsers.add(u);
			return 0;
		}

		return 1;
	}

	public int loadUsers() {
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + "users/__userlist__"))) {
			String buf;
			while((buf = in.readLine()) != null) {
				User u = new User(domain);
				u.loadUser(buf);
			}

			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int loadContent(int contentid) {
		Content c = new Content(domain);
		if(c.loadContent(contentid) == 0) {
			allContents.add(c);
			return 0;
		}

		return 1;
	}

	public int loadContent(String title) {
		Content c = new Content(domain);
		if(c.loadContent(title) == 0) {
			allContents.add(c);
			return 0;
		}

		return 1;
	}

	public int loadContents() {
		try(BufferedReader in = Files.newBufferedReader(Paths.get(path + "contents/__contentlist__"))) {
			String buf;
			while((buf = in.readLine()) != null) {
				Content c = new Content(domain);
				c.loadContent(buf);
			}

			return 0;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return 1;
	}

	public int saveUser(int uid) {
		return searchUser(uid).saveUser();
	}

	public int saveUser(User u) {
		return u.saveUser();
	}

	public int saveUsers() {
		for(User u : allUsers) {
			saveUser(u);
		}
		return 0;
	}

	public int saveContent(int cid) {
		return searchContent(cid).saveContent();
	}

	public int saveContent(String title) {
		return searchContent(title).saveContent();
	}

	public int saveContent(Content c) {
		return c.saveContent();
	}

	public int saveContents() {
		for(Content c : allContents) {
			saveContent(c);
		}
		return 0;
	}

	public User searchUser(int uid) {
		for(User u : allUsers) {
			if(u.getUserid() == uid) {
				return u;
			}
		}

		return null;
	}

	public User searchUser(String id) {
		for(User u : allUsers) {
			if(u.getId().equals(id)) {
				return u;
			}
		}

		return null;
	}

	public Content searchContent(int cid) {
		for(Content c : allContents) {
			if(c.getContentid() == cid) {
				return c;
			}
		}

		return null;
	}

	public Content searchContent(String title) {
		for(Content u : allContents) {
			if(u.getTitle().equals(title)) {
				return u;
			}
		}

		return null;
	}
}
