package kr.ac.kpu.sigongjoa;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class Editor {
	private static final String prompt = " ? ";
	private String buffer;

	private Scanner sc;

	public Editor(Scanner sc) {
		this.sc = sc;
	}

	public String getBuffer() {
		return buffer;
	}

	public void setBuffer(String buffer) {
		this.buffer = buffer;
	}

	public void edit(String data) {
		buffer = data;
loop:
		for(;;) {
			System.out.print(prompt);
			switch(sc.nextLine()) {
				case "help":
					System.out.println("view : show you current buffer");
					System.out.println("n : add new character or word or line or File.");
					System.out.println("d : del one existing character or word or line.");
					System.out.println("clear : clear current buffer.");
					System.out.println("exit : exit from touedit.");
					break;
				case "view":
					System.out.println(buffer);
					break;
				case "n":
					System.out.print(" ! ");
					switch(sc.nextLine()) {
						case "help":
							System.out.println("help : show you this page.");
							System.out.println("char : add new character.");
							System.out.println("word : add new word.");
							System.out.println("line : add new line.");
							System.out.println("file : add new file.");
							break;
						case "char":
							System.out.print("input : ");
							buffer += sc.nextLine();
							break;
						case "word":
							System.out.print("input : ");
							buffer += sc.nextLine().trim();
							break;
						case "line":
							System.out.print("input : ");
							buffer += "\n" + sc.nextLine().trim();
							break;
						case "file":
							System.out.print("file path : ");
							buffer += "\n" + String.join("\n", Files.readAllLines(Paths.get(sc.nextLine().trim())));
							break;
					}
					break;
				case "d":
					System.out.print(" ! ");
					switch(sc.nextLine()) {
						case "help":
							System.out.println("help : show you this page.");
							System.out.println("char : delete last character.");
							System.out.println("word : delete last word.");
							System.out.println("line : delete last line.");
							break;
						case "char":
							if(!buffer.isEmpty()) {
								buffer = buffer.substring(0, buffer.length() - 1);
							}
							break;
						case "word":
							int index = buffer.lastIndexOf(' ');
							index = index < 0 ? buffer.lastIndexOf('\t') : index;
							index = index < 0 ? buffer.lastIndexOf('\n') : index;
							if(index > -1) {
								buffer = buffer.substring(0, buffer.lastIndexOf(' '));
							}
							break;
						case "line":
							int index = buffer.lastIndexOf('\n');
							if(index > -1) {
								buffer = buffer.substring(0, buffer.lastIndexOf(' '));
							}
							break;
					}
					break;
				case "clear":
					System.out.println("Are you sure to clear this buffer? (y/n) ");
					if("y".equals(sc.nextLine())) {
						buffer = "";
					}
					break;
				case "exit":
					break loop;
			}
		}
	}
}
